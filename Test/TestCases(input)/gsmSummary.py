'''
Created on 01-Apr-2013

@author: spam
'''

from __future__ import division
import string
import nltk
import collections
import math
import sys
from collections import Counter
from nltk import cluster
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.corpus import wordnet
from nltk.tokenize import sent_tokenize


def corpusInput(file):
    inp = open(file,'r')
    readInput = inp.read()
    return readInput


def rem_symbols(word):
    exclude = list(set(string.punctuation))
    new_word = ''.join(ch for ch in word if ch not in exclude)
    return new_word


def stop_rem(word):
    englishStop = list(set(stopwords.words('english')))
    if word not in englishStop:
        return False
    else:
        return True


def posTag(sentence):
    words = []
    text = nltk.word_tokenize(sentence)
    tag = nltk.pos_tag(text)
    for each in tag:
        if each[1] in ('NN', 'NNS') and len(each[0]) > 1:
            words.append(each[0])
    return words


def lemmatize(word):
    lmtzr = WordNetLemmatizer()
    return lmtzr.lemmatize(word)


def preProcessing(file):
    sent_words_n = []   # List of list of sentences along with line nos and words which are nouns
    sent_words = []     # List of list of sentences along with words and line nos
    sen_n = []
    sen = []
    nWords = []     # List of Nouns
    sentences = sent_tokenize(corpusInput(file))
    for each in sentences:
        sen.append(each)
        sen.extend(nltk.word_tokenize(each))
        sent_words.append(list(sen))
        sen_n.append(each)
        sen_n.extend(posTag(each))
        nWords.extend(sen_n[1:])
        sent_words_n.append(list(sen_n))
        del sen[:]
        del sen_n[:]
    count = 0
    for each in sent_words_n:
        sent_words_n[count].insert(0,count)
        sent_words[count].insert(0,count)
        count = count + 1

    ## Remove symbols, stop-words and lemmatize (sent_words)
    mCount = 0
    for each in sent_words:
        count = 2
        toDel = []
        for w in each[2:]:
            if stop_rem(each[count]) is True:
                toDel.append(count)
            each[count] = rem_symbols(w)
            each[count] = lemmatize(each[count])
            count = count + 1
        if len(toDel) != 0:
            for n in toDel:
                each[n] = ''
        sent_words[mCount] = list(each)
        mCount = mCount + 1

    ## Remove symbols, stop-words and lemmatize (sent_words_n)
    mCount = 0
    for each in sent_words_n:
        count = 2
        toDel = []
        for w in each[2:]:
            if stop_rem(each[count]) is True:
                toDel.append(count)
            each[count] = rem_symbols(w)
            each[count] = lemmatize(each[count])
            count = count + 1
        if len(toDel) != 0:
            for n in toDel:
                each[n] = ''
        sent_words_n[mCount] = list(each)
        mCount = mCount + 1

    ## Remove null strings from the lists
    count = 0
    for each in sent_words:
        sent_words_n[count] = [i for i in sent_words_n[count] if i != '']
        sent_words[count] = [i for i in sent_words[count] if i != '']
        count = count + 1

    return sent_words_n, sent_words, nWords


def sentenceLength(fValues, sent_words):
    max = 0
    for each in sent_words:
        if len(nltk.word_tokenize(each[1])) > max:
            max = len(nltk.word_tokenize(each[1]))

    count = 0
    for each in sent_words:
        value = len(nltk.word_tokenize(each[1])) / max
        fValues[count].append(value)
        count = count + 1

    return fValues


def termWeight(fValues, sent_words, final):
    words = []
    tfisf = []
    ni = []
    termWT = []
    for each in final:
        words.append(each[0])

    for each in words:
        tc = 0
        for s in sent_words:
            if each in s[2:]:
                tc = tc + 1
        ni.append(tc)

    count = 0
    for each in words:
        wt = final[count][1] * math.log(len(sent_words) / ni[count])
        tfisf.append(wt)
        count = count + 1

    for each in sent_words:
        sum = 0
        temp = []
        for w in each[2:]:
            pos = words.index(w)
            sum = sum + tfisf[pos]
        temp.append(each[0])
        temp.append(sum)
        termWT.append(list(temp))
        del temp[:]

    max = 0
    for each in termWT:
        if each[-1] > max:
            max = each[-1]

    count = 0
    for each in sent_words:
        value = termWT[count][-1] / max
        fValues[count].append(value)
        count = count + 1

    return fValues


def sentencePosition(fValues, sent_words_n):
    if len(sent_words_n) <= 5:
        count = 0
        score = len(sent_words_n)
        for each in sent_words_n:
            value = score / len(sent_words_n)
            fValues[count].append(value)
            score = score - 1
            count = count + 1

    if len(sent_words_n) > 5 and len(sent_words_n) < 10:
        fValues[0].append(5 / 5)
        fValues[1].append(4 / 5)
        fValues[2].append(3 / 5)
        fValues[3].append(2 / 5)
        fValues[4].append(1 / 5)
        fValues[-1].append(5 / 5)

        if len(sent_words_n) > 6:
            for each in range(5, (len(sent_words_n) - 1)):
                fValues[each].append(0 / 5)

    if len(sent_words_n) is 10:
        fValues[0].append(5 / 5)
        fValues[1].append(4 / 5)
        fValues[2].append(3 / 5)
        fValues[3].append(2 / 5)
        fValues[4].append(1 / 5)
        fValues[-5].append(1 / 5)
        fValues[-4].append(2 / 5)
        fValues[-3].append(3 / 5)
        fValues[-2].append(4 / 5)
        fValues[-1].append(5 / 5)

    if len(sent_words_n) > 10:
        fValues[0].append(5 / 5)
        fValues[1].append(4 / 5)
        fValues[2].append(3 / 5)
        fValues[3].append(2 / 5)
        fValues[4].append(1 / 5)
        fValues[-5].append(1 / 5)
        fValues[-4].append(2 / 5)
        fValues[-3].append(3 / 5)
        fValues[-2].append(4 / 5)
        fValues[-1].append(5 / 5)

        for each in range(5, (len(sent_words_n) - 5)):
            fValues[each].append(0 / 5)

    return fValues


def sentenceSimilarity(fValues, sent_words):
    def buildVector(iterable1, iterable2):
        counter1 = Counter(iterable1)
        counter2 = Counter(iterable2)
        all_items = set(counter1.keys()).union(set(counter2.keys()))
        vector1 = [counter1[k] for k in all_items]
        vector2 = [counter2[k] for k in all_items]
        return vector1, vector2

    sSim = []
    eCount = 0
    count = 0

    for each1 in sent_words:
        temp = []
        sum = 0
        for each2 in sent_words:
            if each1[1] != each2[1]:
                v1,v2 = buildVector(each1,each2)
                sum = sum + cluster.util.cosine_distance(v1,v2)

        temp.append(eCount)
        temp.append(sum)
        sSim.append(list(temp))
        eCount = eCount + 1

    max = 0
    for each in sSim:
        if each[-1] > max:
            max = each[-1]

    for each in sSim:
        value = each[-1] / max
        fValues[count].append(value)
        count = count + 1

    return fValues


def nounCluster(fValues, sent_words_n):
    finalLinked = []    # List of list of closest nouns
    fLinked = []        # List of list of longest lists in finalLinked
    linked = []
    words = []
    cWords = []
    count = 0

    for each in sent_words_n:
        for n in each[2:]:
            words.append(n)

    temp = list(set(words))
    max = 0
    del words[:]
    for each in temp:
        try:
            w = each.lower() + ".n.01"
            syn = wordnet.synset(w)
            words.append(each)
        except:
            pass

    for word in words:
        del linked[:]
        min = 999999999
        linked.append(word)
        w1 = word + ".n.01"
        syn1 = wordnet.synset(w1)
        for each in words:
            if each != word:
                w2 = each + ".n.01"
                syn2 = wordnet.synset(w2)
                dist = syn1.shortest_path_distance(syn2)
                if dist < min:
                    min = dist
                    del linked[:]
                    linked.append(word)
                    linked.append(each)
                elif min == dist:
                    linked.append(each)
        linked.append(min)
        finalLinked.append(list(linked))

        ## List containing the lists with maximum
        ## number of nouns closest to each other
        if len(linked) > max:
            max = len(linked)
            del fLinked[:]
            fLinked.append(list(linked))

    for each in finalLinked:
        for l in fLinked:
            distance = l[-1]
            if each[0] in l[1]:
                if each[-1] <= distance:
                    fLinked.append(list(each))

    for each in fLinked:
        for w in each[:-1]:
            cWords.append(w)

    cWords = list(set(cWords))
    for each in sent_words_n:
        cCount = 0
        for w in each[2:]:
            if w in cWords:
                cCount = cCount + 1
        try:
            value = cCount / (len(each) - 2)
            fValues[count].append(value)
            count = count + 1
        except:
            value = (0 / 5)
            fValues[count].append(value)
            count = count + 1

    return fValues


def hypernym(fValues, sent_words_n):
    words = []
    hypWord = []
    con = []
    for each in sent_words_n:
        for w in each[2:]:
            words.append(w)

    for each in words:
        testWord = each + ".n.01"
        try:
            syn = wordnet.synset(testWord)
            hypernyms = syn.hypernym_paths()
            hyp = hypernyms[0][-2]
            hypWord.append(hyp.lemmas[0].name)
        except:
            pass

    counter = collections.Counter(hypWord)
    final = counter.most_common(None)
    maximum = final[0][1]

    for each in final:
        if each[1] in [maximum, maximum-1, maximum-2, maximum-3]:
            con.append(each[0])

    count = 0
    for each in sent_words_n:
        hCount = 0
        for w in each[2:]:
            testWord = w + ".n.01"

            try:
                syn = wordnet.synset(testWord)
                hypernyms = syn.hypernym_paths()
                hyp = hypernyms[0][-2]
                if hyp.lemmas[0].name in con:
                    hCount = hCount + 1
            except:
                pass

        try:
            value = hCount / (len(each) - 2)
            fValues[count].append(value)
            count = count + 1
        except:
            value = (0 / 5)
            fValues[count].append(value)
            count = count + 1

    return fValues


def properNoun(fValues, sent_words):
    count = 0
    for each in sent_words:
        text = nltk.word_tokenize(each[1])
        tag = nltk.pos_tag(text)
        tCount = 0
        for t in tag:
            if t[1] in ('NNP', 'NP'):
                tCount = tCount + 1

        value = tCount / len(text)
        fValues[count].append(value)
        count = count + 1

    return fValues


def thematicWord(fValues, sent_words):
    words = []
    tWords = []     # Top 5 Thematic words
    for each in sent_words:
        words.extend(each[2:])

    counter = collections.Counter(words)
    final = counter.most_common(None)

    if len(final) > 5:
        for each in range(0,5):
            tWords.append(final[each][0])
    else:
        for each in range(0,len(final)):
            tWords.append(final[each][0])

    count = 0
    for each in sent_words:
        tcount = 0
        mW = list(each[2:])
        for t in tWords:
            if t in mW:
                tcount = tcount + 1

        value = tcount / len(tWords)
        fValues[count].append(value)
        count = count + 1

    return fValues, final


def numericalData(fValues, sent_words):
    count = 0
    for each in sent_words:
        text = nltk.word_tokenize(each[1])
        nCount = 0
        for w in each[2:]:
            if ('0' in w) or ('1' in w) or ('2' in w) or ('3' in w) or ('4' in w) or ('5' in w) or ('6' in w) or ('7' in w) or ('8' in w) or ('9' in w):
                nCount = nCount + 1
        value = nCount / len(text)
        fValues[count].append(value)
        count = count + 1

    return fValues


def cuePhrases(fValues, sent_words, cue):
    phrases = []
    inpCue = open(cue, 'r')

    for line in inpCue.read().splitlines():
        phrases.append(str(line))

    if len(phrases) != 0:
        count = 0
        for each in sent_words:
            pCount = 0
            for w in each[2:]:
                if w in phrases:
                    pCount = pCount + 1

            value = pCount / len(phrases)
            fValues[count].append(value)
            count = count + 1

    return fValues


def summaryList(fValues, percentage):
    sumList = []
    lineList = []
    for each in fValues:
        temp = []
        sum = 0.0
        for num in each[1:]:
            sum = sum + float(num)
        temp.append(each[0])
        temp.append(sum)
        sumList.append(temp)

    sumList = list(sorted(sumList, key=lambda sum: sum[1], reverse=True))
    for each in sumList:
        lineList.append(each[0])

    noLines = int(math.ceil((float(percentage) / 100) * len(lineList)))
    del lineList[noLines:]
    lineList = list(sorted(lineList))
    
    return lineList


def summaryGenerate(lineList, sent_words, file, percentage):
    out = open(str(file[:-4]) + "(summary)" + str(percentage) + ".txt", 'w')
    for each in sent_words:
        if each[0] in lineList:
            out.write(each[1] + "\n")
    out.close()


def context(file, cue, percentage):
    print "Processing..."
    fValues = []
    sent_words_n, sent_words, nWords = preProcessing(file)

    for each in sent_words_n:
        fValues.append([each[0]])

    fValues = sentenceLength(fValues, sent_words)
    fValues = properNoun(fValues, sent_words)
    fValues = numericalData(fValues, sent_words)
    fValues, final = thematicWord(fValues, sent_words)
    fValues = termWeight(fValues, sent_words, final)
    fValues = hypernym(fValues, sent_words_n)
    fValues = nounCluster(fValues, sent_words_n)
    fValues = sentenceSimilarity(fValues, sent_words)
    fValues = sentencePosition(fValues, sent_words)
    fValues = cuePhrases(fValues, sent_words, cue)
    lineList = summaryList(fValues, percentage)
    summaryGenerate(lineList, sent_words, file, percentage)
    print "\nProcessing Complete..."
    print "\nOpen " + str(file[:-4]) + "(summary)" + str(percentage) + ".txt" + " to read the summary..."


def main():
    context(sys.argv[1],sys.argv[2],sys.argv[3])

if __name__ == '__main__':
    main()
