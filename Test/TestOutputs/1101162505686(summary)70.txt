The sounds of traditional Mexican music will fill the halls of the Capitol Theatre next weekend, as the Masters of Mexican Music bring the sounds of mariachi, marimba and tejano music to the stage.
The Masters of Mexican Music is a collaboration of accomplished artists.
In its first national tour, Masters of Mexican Music immerses audiences in the rich musical tradition of Mexico.
The performances include Mariachi Los Camperos de Nati Cano, which will bring mariachi music to the stage; Jose Gutierrez y Los Hermanos Ochoa, who will perform musica jarocha style music; Mingo Saldivar y Los Cuatro Espadas, who will perform conjunto tejano music; and Marimba Chiapas, which will perform traditional marimba-style music.
Mariachi Los Camperos de Nati Cano is a group led by Natividad Cano, who was born into a family of mariachi musicians in Ahuisculco, Jalisco, Mexico in 1933.
It wasn't until he moved to Los Angeles that he founded the group Los Camperos, which he has been directing for more than 40 years.
Jose Gutierrez y Los Hermanos Ochoa is a group led by Jose Gutierrez Ramun, who was born in Costa de la Palma, Mexico.
Mingo Saldivar y Los Cuatro Espadas is a group from San Antonio, Texas.
It wasn't too long after taking up the guitar that Saldivar learned to play the stand-up base and a year later he took up the accordion.
Marimba Chiapas is a group led by Lorenzo Cruz, who was born in Tonal, Chiapas, Mexico in 1928.
He was taught to play the marimba by Felipe Peoa.
A marimba is constructed similarly to the xylophone and is often referred to as the "Mexican piano.
" In 1973, Cruz moved to the United States, where he has continued to sustain the Mexican tradition of marimba music.
Although the musical troupe that makes up the Masters of Mexican Music will perform Sunday, Nov. 7, at the Capitol Theatre, they will be in the Yakima Valley for three days performing at different locations.
Saturday, Nov. 6, the troupe will perform at various outreach locations in the Valley, including a free performance at 2:30 p.m. at St. Joseph's Catholic Church in Alderwood.
Later that night Jose Gutierrez y Los Hermanos Ochoa will perform at Radio KDNA's 25th anniversary celebration at the Yakima Convention Center.
Monday, Nov. 8, Marimba Chiapas will work with students at Granger High School at 9:30 a.m. At 1 p.m. the same day Mariachi Los Camperos de Nati Cano will work with students at Grandview High School.
Sunday's performance at the Capitol Theatre is scheduled to take place at 3 p.m. Tickets for the Masters of Mexican Music are currently on sale through the Capitol Theatre box office at (509) 853-ARTS and through radio KDNA in Granger at (509) 854-1900.
