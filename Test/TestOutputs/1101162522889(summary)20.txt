Selling change is always difficult, but when it involves public schools and our kids' education, it is daunting at best.
Dr. Rod Paige is President Bush's Secretary of Education and the chief architect of "No Child Left Behind.
Born in 1933 in segregated Monticello, Mississippi, Paige earned a Bachelor's degree from Jackson State University and a Master's and Doctoral degree from Indiana University.
For example, while in Seattle recently, he conducted a town hall meeting where teachers and activists confronted him over their frustrations with Washington's school reforms, the Washington Assessment of Student Learning (WASL), and "No Child Left Behind.
Paige also believes test scores are a measure of where students have problems, and he rejects the notion of averaging school and district test scores as an indicator of progress.
Locke, Education Secretary Rod Paige is convinced that education breaks down racial, cultural and poverty barriers.
