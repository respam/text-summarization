
For more than 20 years Linda Roberson has been providing electrolysis services to Lower Valley residents seeking permanent hair removal.
Most of her career, Roberson has practiced out of a beauty shop on Seventh Street in Alderwood, but about four years ago when New Image moved to Edison Avenue, so did Roberson.
"In our family we have people who have a lot of hair and you can't work on yourself, so when the opportunity came up we both took the classes," said the Mabton resident.
"It's a rewarding field," said Roberson, who finds a lot of satisfaction in helping women who are uncomfortable with facial hair or other problem areas.
"As of today, electrolysis is the only method of permanent hair removal that is recognized by the Food and Drug Administration," said Roberson.
She said plucking, tweezing, creams, waxes and other methods of hair removal don't kill the follicle and instead draw more blood to the area where hair is removed, causing it to grow back thicker and darker.
Although some people think that electrolysis is similar to a medical procedure, Roberson is quick to let all know that they are not medical persons, but she added it is not the "back door business" people used to think it was.
She recently returned from an electrolysis conference in Las Vegas, where she heard from medical professionals on different things to look for on client's skin and was updated on new technologies.
Roberson said she often sees or notices changes in moles that a client may not know about.
On a few occasions Roberson's suggestion to go to the doctor has proven to benefit her clients, who were found to have cancerous spots on their faces.
"This is not like getting a shot," she tells her clients.
"I go along the hair follicle and use a high frequency heat machine to cook the hair from the inside out.
"   Roberson said her clients keep her interested in continuing electrolysis.
"When I see my clients on the street walking with their head high, it makes me feel good," said Roberson.
