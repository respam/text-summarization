The home of Eva Martinez was festive this past Saturday night as she welcomed her sons home from Iraq.
A tearful homecoming with family began about 4 p.m. when her sons arrived at her Grandview home.
It had been a year since Martinez saw her oldest son, Sgt.
Ismael Ramirez, 25, and six months since Specialist Job Ramirez, 24, had visited home, according to the brothers' sister, Socorro Ramirez.
The Ramirez brothers each received a four-day weekend and decided to visit their mother and other family in the Lower Valley.
The two brothers are in the same Striker Brigade combination team, the 3rd Brigade of the Second Infantry Division.
Both work in supply and were actually at the same camp in Iraq for a while before Ismael was promoted and moved about 50 kilometers away.
"Iraq is colder and rainier than everybody thinks it is," said Ismael, who added that they had two days of snow before he left Iraq.
Stationed in the city of Mosul, which is a huge sprawling city, he said that although they are trained to work in supply, they, like other military personnel, had duties that aren't typical of supply officers.
Being in Mosul for a year, Ismael has seen differences made in the services to the military personnel, as well as the citizens of the city.
Returning home, the brothers came to the Valley to be surrounded by family and friends, whom they haven't seen in a while.
"Once we're back it's like a regular day job," said Ismael.
"We were last together a year ago last July when my brother (Ismael) got married.
"   One of her biggest fears is that now that her brother, Job, has re-enlisted, he will be required to go back to Iraq, although he believes he will have some time home before returning there.
