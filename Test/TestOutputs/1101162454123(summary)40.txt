Last Friday's dominating 35-7 victory over Entiat earned the Mabton High School football team a berth in this year's 'B' State playoffs.
Mabton, which lost its first two league games of the season but bounced back to win six straight in conference play, will open the postseason against the Lind-Ritzville Broncos.
The Vikings' first round State playoff opponent won the Northeast 'B' League with a perfect 9-0 record.
Search said the site and date of his team's first round playoff game against Ritzville is not yet decided.
Mabton QB Ryan Harris completed 11-of-21 passes against Entiat, serving up only one interception while throwing for 174 yards.
He's progressively gotten better as the season has gone along," Search said.
Gutierrez and Pablo Sanchez, who caught two passes for 32 yards and also rushed for 60 yards on 13 carries, each caught a touchdown pass from Harris.
Junior Ray Elizondo was the workhorse for the Vikes, taking 20 hand-offs from Harris and picking up a season best 167 yards.
"It was Ray's biggest game of the season," said Search, explaining that Entiat didn't adjust to Mabton's dives up the middle.
The reason Elizondo had so much room in the middle to run, said Search, was because of the play of the Vikes' offensive line.
The Vikings increased their lead to 14-0 in the second period, on a 16-yard touchdown run by Alex Zavala, who carried the ball 11 times Friday night, tallying 59 yards in the process.
After Entiat picked off a Harris pass, at Mabton's 22-yard line, the home team notched its only score of the night to draw to within 14-7.
Defensively, Search said his Vikes turned in another splendid performance.
"Other than Entiat's opening drive, when they got to our 4-yard line, and the one drive after they intercepted our quarterback, they never really challenged us again," said Search.



