
Nora �Marlen� Gonzalez, 11, of Alderwood died Thursday, Oct. 28, 2004, at Harborview Medical Center in Seattle.
She was born on Oct. 22, 1993 in Prosser, the daughter of Nora (Salinas) and Oscar Gonzalez.
She is survived by her parents, Oscar and Nora Gonzalez of Alderwood; one brother, Oscar Gonzalez Jr.; two sisters, Leslie Brighit Gonzalez and Ichnic Maria Gonzalez, all of Alderwood; her grandparents, Maria Guajardo of Texas, Elizar Gonzalez of Mattawa, Francisco and Maria B. Salinas of Mexico; and aunts, uncles and cousins.
Viewing and visitation will be held Monday, Nov. 1, 2004, from 10 a.m. to 8 p.m. with a Rosary to be recited at 6 p.m. at Wells Funeral Home, Alderwood.
He will always be missed and in the hearts of his three daughters, Irma Martinez of Alderwood, Sandy Cavagnaro of Seattle and Gracie Bravo of Grandview; his three sons, Aurtio Cruz and wife, Maria, of Grandview, Joe Cecil Cruz of Prosser and John Cruz of Alderwood.
He is also survived by 18 grandchildren, 19 great-grandchildren and seven great-great-grandchildren.
Viewing for Mr. Cruz will be held Monday, Nov. 1, 2004, from 4 to 8 p.m.
with a Rosary to be recited at 7 p.m. at Valley Hills Funeral Home Chapel, corner of North Ave. and 11th Street, Alderwood.
Viewing will also be held Tuesday, Nov. 2, 2004 from 9 a.m. to 1 p.m. with a speaker, Glen Steffin, at l p.m. Graveside funeral services will follow at Lower Valley Memorial Gardens, Alderwood, at 2 p.m. Valley Hills Funeral Home is entrusted with arrangements.
�
WILLEM HUBERS
Willem �Bill� Hubers, 73, of Alderwood passed away Saturday, Oct. 30, 2004, at Alderwood Community Hospital.
Bill is survived by three sisters, Barbara Hubers, Adri Clapp and husband, John, and Symina Meeker, all of Alderwood; three nephews, Lawrence Meeker and wife, Joyce, Clarence Meeker and wife, Pam, and Willard Meeker; five nieces, Elaine MacDougall and husband, Jeff, Edie Graf and husband, George, Jana Sall and husband, Paige and Beth Clapp; six great-nieces; and two-great nephews.
Viewing and visitation will be held Tuesday, Nov. 2, 2004, from 2 to 8 p.m. at Wells Funeral Home, Alderwood.
Funeral services will be held Wednesday, Nov. 3, 2004, at 10 a.m. at Wells Funeral Home, Alderwood.
