
Part of the next generation of clowns attended the first of several classes last night to learn the basics and secrets of how to become a clown.
Monday night at the Alderwood Beauty Academy, Arlo Waggoner, who is also known to many as Waldo the Clown, started the class by showing the students a video by the Ringling Brother's Barnum and Bailey Clown College.
The video covered the basics of what the students will be learning over the next several weeks as they delve into the clowning world.
A form of entertainment that has been around for more than a thousand years, clowns were once known as court jesters.
Waggoner said the first step is to learn how to move like a clown.
Although they will have time to practice later, the students also learned the basics of juggling, which includes starting out with something like a light handkerchief, which will fall slowly.
Waggoner said as that becomes easier, begin trying with soft bean bags, which won't roll away if dropped.
The students also learned there are three different kinds of clown make-up faces, the white faced clowns, which are the more reserved clowns.
Waggoner said that if his students choose to be a tramp or character clown he wants them to dress nicely instead of in old rags.
Clowns use color on their lower lip only and smiles are generally smaller, not the big scary smiles often created by inexperienced clowns, said Waggoner.
"We use plain old baby powder as long as it doesn't have cornstarch in it," said Waggoner, adding that they brush off excess powder with a brush.
"After I get my make-up on I can go swimming and it won't come off," said Waggoner.
The next class will be held Monday, Nov. 15, at the Alderwood Beauty Academy.
Waggoner said it's not too late to sign up for the free classes.
The class is open to all ages.
