In an effort to gain space for extra classrooms, the Mabton School Board approved the purchase of a portable unit during this past Monday's board meeting.
The new unit will add two classrooms to the campus at the Mabton Jr.-Sr. High School building, giving math teachers Ron Fleming and John Durham regular classrooms, said Keith Morris, high school principal.
Fleming's makeshift classroom is located within a room near the district bus garage and Durham is now utilizing the district board room as a classroom, Morris explained.
The addition of the two classrooms will cost the district an estimated $100,000, said Mabton Superintendent Sandra Pasiero-Davis.
She said the cost also includes such things as setting the unit upon a new foundation, and electrical and water needs.
The unit will be situated at the south end of the high school parking lot and should be ready for use 10 weeks after the unit is ordered, Pasiero-Davis said.
In other action taken at its Oct. 25 meeting, the board approved a safe school/healthy students agreement with Educational Services District 105.
Pasiero-Davis said the agreement will provide case management services to the Mabton School District.
The case manager will be housed in the Mabton School District and will work full time with Mabton students.
