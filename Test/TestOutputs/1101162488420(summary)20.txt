This year the ballot measures gave voters a chance to decide on the fate of everything from the presence of electronic scratch tickets at licensed non-tribal gaming establishments to the creation of an education trust fund in the state.
This mirrors the response from voters across Washington state, where 59.9 percent voted for the initiative.
Initiative 884   Initiative 884 asked voters to approve the creation of an education trust fund by increasing the sales tax across the state by 1 percent.
Tuesday, voters in Washington voted down Referendum Measure 55, which would have made it possible to establish charter schools in the state.
Again, the percentage of voters who voted against Referendum 55 in Washington state is almost exactly the same as the percentage of those who voted against it in Yakima County.
In Yakima County, 61.66 percent of voters voted for the measure, compared to 68.51 percent who approved it across the state.
1   Locally, Yakima County voters approved Proposition No.
Throughout Yakima County, 56.04 percent of voters voted for the proposition.
The proposition is expected to raise $6.1 million annually for the Yakima County criminal justice system, with the funds being dispersed to law enforcement agencies throughout the county.
