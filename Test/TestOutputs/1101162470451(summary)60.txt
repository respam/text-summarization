
Strange, the people we meet in our everyday lives.
These are the people who don't realize they are entwined in our existences.
They are the people we don't really know, but whom we see almost weekly, maybe daily.
The people I speak of might be that clerk that sells you your soda before you head off to work, greeting you with a friendly "good morning," or the lady at the latte stand, who always asks how your day is going.
Maybe it's the guy who picks up your garbage, who always has a joke or a chuckle at the ready, or the lady in church whom you smile at and nod to every Sunday, the one who provides you with a brief respite by constantly whispering in her four children's ears to quit scuffling and sit up straight.
These people aren't our family members, most times we can't even call them friends.
They're just people, acquaintances so to speak, people who don't realize that they're an integrated part of our lives.
We all have such people in our lives.
These aren't the people to whom we vent our built-up frustrations, complain to about the boss at work who's making our lives miserable or chew out for the cruddy dinner that was served up the previous night.
No, these are the people we hardly know, yet we are always polite to them, always talk civil to them and always put on our best face for them.
Why is it that for these people, the ones we barely know, we can always be on our best?
We're not always pleasant with our loved ones...we snarl, we grumble, we complain, we question, we yell, we curse.
There are times we don't even try to act human with the people who mean the most to us...we don't think to offer up even a flimsy compliment to our wife or husband, or tell our son or daughter what a great job they did in scoring a B on this week's history test.
From someone who knows, from someone who regrets their past actions with loved ones and has lamented relationships that are no more, value your family.
Put stock in what you have, and treat those who mean the most to you with the utmost respect they deserve.
