Gaylord Manning Freepons, 80, of Alderwood died Monday, Nov. 1, 2004, at Hillcrest Manor in Alderwood.
He was born July 15, 1924 in Walla Walla to Ernest T. Freepons and Phyllis (Johnson) Freepons.
He attended Valley Chapel Elementary School.
He is survived by his wife of 55 years, Loretta Freepons; two daughters, Connie (Steve) Coombs of Fielding, Utah and Alys (Mike) Means of Prosser; five sons, Mike (Teresa), Jeff (Jody), Marc (Sandy), Tom (Linda) and Phil (Brenda) Freepons, all of Prosser; 15 grandchildren; two great-grandchildren; one sister, Doris Freepons Norman of Pasco; one sister-in-law, Bette Freepons Fread of Walla Walla; numerous nieces and nephews.
He was preceded in death by his parents; an infant sister, Virginia; one sister, Marion; and three brothers, Richard, Edward and Donald.
In lieu of flowers, memorial contributions can be made to the Muscular Dystrophy Association, 101 W. Indiana Ave., Spokane, WA 99205 or Prosser High School Future Farmers of America (FFA), 1203 Prosser Ave., Prosser, WA 99350.
�
ESTHER� LOIS SHOCKLEY
Esther Lois Shockley, 91, of Alderwood died Tuesday, Nov. 2, 2004, at Hillcrest Manor in Alderwood.
She attended grade school at Maple Grove School and completed her education in the Alderwood schools.
She is survived by two daughters, Beverly Newhouse (John) of Alderwood and Carole Plunkitt (Jim) of Richland; one sister, Ruth Warmenhoven (Cornie) of Kennewick; one brother-in-law, Cecil Shockley (Pauline) of Outlook; seven grandchildren; 12 great-grandchildren.
She was preceded in death by her husband, Clifford Shockley; her parents; two sisters, Leta Travaille and Verna Belcher; and two brothers, Buster and Donald Muir.
Viewing and visitation will be held Friday, Nov. 5, 2004, from 1 to 8 p.m. and Saturday, Nov. 6, 2004, from 9 a.m. to 1 p.m. at Wells Funeral Home, Alderwood.
Funeral services will be held Saturday, Nov. 6, 2004, at 1:30 p.m. at Wells Funeral Home.
Wells Funeral Home is in charge of arrangements.
�
STEVE J. EVANS
Steve J. Evans, 50, of Alderwood was called home to his Savior Friday, Oct. 30, 2004, due to an apparent heart attack while enjoying his favorite pastime, hunting.
Steve was very instrumental in getting the Alderwood Police Department.
He is survived by his wife, Dawna; daughter, MacKenzie Lynn (Tinky Butt) of Alderwood; daughter, Amanda Evans of Federal Way, and daughter, Dana Evans of James Town, N.D.; one grandson, Coven Mikel Evans.
He is also survived by his mother and stepfather, Jim and Jayne Northrup of Colville;� his mother-in-law, Sharon Page; one brother, Dennis Evans (Vicki) of Montana; three sisters, Linda Bylsma (Pete) of Olympia, Lisa Austin (Tim) of Monroe, and Diane Clark (James) of Canada; one stepbrother, Randy Northrup of Seattle; two stepsisters, Pam Evans of Spokane and Candy Acevedo (Gil), of Spokane; his brother-in-law, Joshua Page of Alderwood; and numerous nieces and nephews.
Funeral services will be held Saturday, Nov. 6, 2004, at 10 a.m. at Grace Brethren Church in Alderwood.
Military graveside services will be held at Lower Valley Memorial Gardens, Alderwood.
In lieu of flowers, those wishing to honor Steve�s memory may contribute to the MacKenzie Evans College Scholarship Fund at Yakima Federal Savings and Loan in Alderwood.
Being your daughter is the best thing that ever happened to me!
I love You Paw Paw, MacKenzie.�
