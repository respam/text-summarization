
When the audience sits down to enjoy the Alderwood High School performance of Romeo and Juliet, they should be prepared to see some modern-day touches.
This year when the local drama students take to the stage they may be speaking Shakespearean language, but there will be no denying that the play is set in modern times.
For the past two and a half months the more than 30 cast members have been working diligently, learning their lines and preparing to bring Romeo and Juliet to the stage.
Drama teacher Maisy Sylling noted that she tries to incorporate a Shakespearean play into the high school's repertoire at least once every four or five years in order to challenge her students to learn the language.
As for why she chose to do Romeo and Juliet this year, Sylling said that she feels it's a timeless story and one with which students can relate.
"It's more powerful if it's set in present day," Sylling said, noting that it makes it easier for students to relate to.
The cast of Romeo and Juliet is made up of about 30 students, with another 25 students working behind the scenes.
Sylling said this season's play helped attract a lot of new students to the drama program.
She noted that for Tony Magana, who will be playing Romeo, this marks the first time he has been on stage.
The play is set to take to the stage at 7 p.m. on Friday and Saturday, Nov. 12 and 13, and Saturday, Nov. 20.
