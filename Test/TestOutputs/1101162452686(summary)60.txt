
Alderwood pharmacist earns Doctorate degree
Story by: Jamie Smart
Date Published to Web: 11/9/2004
Working full time and studying for the past four years has resulted in Thuha "Bac" Nguyen earning the right to add doctor to his name.
But the title doesn't mean he will be seeing patients as a physician, he explains.
"I do, however, have a very extensive knowledge of drug therapy and will be of service to doctors and their patients in helping to monitor their medications," he explained.
Nguyen, who has worked in the Alderwood Community Hospital pharmacy since 1996, recently earned his Doctorate of pharmacy from Idaho State University.
He completed the two-year doctorate program over the course of four years, working as a part-time student while maintaining his job working with fellow hospital pharmacist Arie Van Wingerden of Alderwood.
Nguyen, who came to America in 1987 from Vietnam, earned his pharmacy degree from St. John's University in New York State, before moving to the West Coast.
As a doctor of pharmacy, he doesn't actually prescribe medications to patients, but serves as a resource for physicians and hospital staff.
With the additional training, Nguyen said he is now more qualified to aid physicians with drug therapy and drug monitoring.
A Grandview resident, Nguyen said he worked in Seattle for about six months at a Payless pharmacy.
"I wanted to work in a hospital in a rural setting.
This area and the hospital setting suits me," he added.
