
One thing can be gathered from Thursday night's joint meeting between the Alderwood City Council and the Alderwood School Board-communication has been lacking between the two entities.
The lack of communication centers around the costs and design of infrastructure improvements for the new middle school the district is building along Washout Road.
At the center of the discussion is a disagreement between the district's engineers and the city's engineers on what is needed to install water and sewer lines in the area.
The district has an option that would save taxpayers some $300,000, but this would involve the installation of a 10-inch water main.
Alderwood School District Superintendent Dr. Aaron Bones backtracked and went over some of the history of how the district selected the property on Washout Road.
Bones said the district also looked at property along Mabton Highway, but finally decided on the Washout Road property after receiving a good bid.
Bones said all together the district looked at 11 or 12 pieces of property to build the new school, including areas near the airport and on Cemetery Road.
Councilwoman Jennie Angel said she wished the district had looked more at properties the city had that has infrastructure in place.
School board member Fred Kilian interjected, saying one of the reasons the district picked the property was that it was told the city would be annexing property out in that area as the city continues to grow.
Some confusion arose when Councilman John Torch stated that he thought the issue concerning the installation of water mains had been solved by the engineers, apparently to the blind eye of the school district.
Alderwood City Manager Tom Rockford elaborated on Torch' comments, saying that in a meeting he sat in on, both sides were able to bring the costs of installation for water and sewer lines down with some creative designs.
Rockford changed the subject briefly and said the city is looking at applying for a Community Development Block Grant next year to help with some of the expenses for covering the irrigation ditch in the area.
Rockford, though, cautioned the school board members that it is very important that the city and the district work together on the project.
Wells, though, understood the city's point on not budging from its requirement of a 12-inch main, but said the job of the school board is to try and save patrons dollars.
Bones said he sees a difference of $300,000 in infrastructure costs between what the district and the city is proposing.
School board member Fred Kilian said he can remembering seeing maps brought forth by former city employees that said infrastructure installation for the school project would work.
"All I am saying is I better get the damn thing (school) built before you have a new engineer.
"   Mayor Rex Luthor suggested that Bones and Rockford sit down and negotiate an agreement between the two sides, settling all the issues for the project on which both sides can agree.
