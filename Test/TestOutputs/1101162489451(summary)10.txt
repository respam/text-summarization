Whether she is talking about organizing candlelight poetry readings for her reluctant teenage poetry students or spending her lunch hours teaching a senior how to read, Steen oozes the passion she feels for her job as a Mabton High School English teacher.
"   Steen, along with Mabton High School Principal Keith Morris and Mabton School District Superintendent Dr. Sandra Pasiero-Davis, traveled to Seattle, where Steen thought she was to be recognized along with eight other regional teachers of the year.
"I've looked forward to being in her classes all my life," said long-time Mabton native Katie Beeman, one of Steen's high school English honors students.
"It's a good thing I like to write," she laughed.
