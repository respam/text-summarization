'''
Created on 04-Apr-2013

@author: spam
'''

from nltk.tokenize import sent_tokenize
import os

def main():
    dirList = os.listdir("D:\TextSummarization\WordOutput(40)")
    for each in dirList:
        count = 1
        inp = open(each,'r')
        out = open(each[:-4] + ".html", 'w')
        readInput = inp.read()
        sentences = sent_tokenize(readInput)
        out.write("<html>\n<head><title>" + each[:-4] + ".html" + "</title></head>\n<body bgcolor=\"white\">\n")
        for sen in sentences:
            out.write("<a name=" + "\"" + str(count) + "\"" + ">[" + str(count) + "]</a> <a href=\"#" + str(count) + "\"" + " id=" + str(count) + ">" + sen + "</a>\n")
            count = count + 1

        out.write("</body>\n</html>")

if __name__ == '__main__':
    main()
