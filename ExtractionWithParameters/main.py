'''
Created on 21-Oct-2012

@author: spam
'''
import os
import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import *
from nltk.tokenize import sent_tokenize
import collections
import re

def stripping(words):
  index = 0
  for each in words:
    match = re.search(r'[\w+][^\w+][\w+]',each)
    
    if match:
      words.pop(index)
      
    elif each[-1] == ".":
      j = each.replace(each[-1],"")
      words.pop(index)
      words.insert(index, j)
      
    elif each[0] == ".":
      j = each.replace(each[0],"")
      words.pop(index)
      words.insert(index, j)
      
    index = index + 1
  return words

def main():
  stripped = []
  words = []
  hypWord = []
  con = []
  
  path = "C:\\Users\\spam\\workspace\\nlpResearch\\inputs\\testInputs"
  files = os.listdir(path)
  lmtzr = WordNetLemmatizer()
  englishStop = set(stopwords.words('english'))
  
  # Processing each file in the input directory
  for each in files:
    inp = open(path + "\\" + each,'r')
    readFile = inp.read()
    
    sentenceList = sent_tokenize(readFile)
    wordList = nltk.word_tokenize(readFile.lower())
    
    # POS tagging each word in the wordlist
    taglist = nltk.pos_tag(wordList)
    for tag in taglist:
      if tag[1] in ["NN", "NNS", "NNP"]:
        words.append(tag[0])
        
    # Lemmatizing
    for every in words:
      strip = lmtzr.lemmatize(every)
      stripped.append(strip)
      
    # Removing punctuation
    stripped = stripping(stripped)
    ##print stripped
    
    # Removing stop words from the list
    index = 0
    for w in stripped:
      if w in englishStop:
        stripped.pop(index)
      index = index + 1
    ##print stripped
    
    # List of all the unique words
    unique_stripped = set(stripped)
    ##print unique_stripped
    
    
    
if __name__ == '__main__':
    main()