'''
Created on 22-Oct-2012

@author: spam
'''
import re

def stripping(words):
  index = 0
  for each in words:
    match = re.search(r'[\w+][^\w+][\w+]',each)
    
    if match:
      words.remove(each)
      
    elif each[-1] == ".":
      j = each.replace(each[-1],"")
      words.remove(index)
      words.insert(index, j)
      
    elif each[0] == ".":
      j = each.replace(each[0],"")
      words.remove(index)
      words.insert(index, j)
      
    index = index + 1
  return words