#@PydevCodeAnalysisIgnore
'''
Created on 04-Jul-2012

@author: spam
'''

import sys
from urllib import urlopen
from nltk.tokenize import *
from nltk.corpus import *

def comprehensive(outFileName):
  tokenizer = TreebankWordTokenizer()

  url = "http://www.textfiles.com/100/cDc-0200.txt"
  inp = urlopen(url).read()
  out = open(outFileName,'w')
  
  #List of sentences
  sentenceList = sent_tokenize(inp)

  for line in sentenceList:
    out.write(line)
    
  out.close()

def main():
  comprehensive(sys.argv[1])

if __name__ == '__main__':
  main()