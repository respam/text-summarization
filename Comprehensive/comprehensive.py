'''
Created on 04-Jul-2012

@author: spam
'''

import sys
from nltk.tokenize import *
from nltk.corpus import *

def comprehensive(inpFileName, inpWord, outFileName):
  tokenizer = TreebankWordTokenizer()

  synonyms = []
  statements = []
  for syn in wordnet.synsets(inpWord):
    for lemma in syn.lemmas:
      synonyms.append(lemma.name)

  inp = open(inpFileName, 'r')
  out = open(outFileName,'w')
  readFile = inp.read()

  #List of sentences
  sentenceList = sent_tokenize(readFile)

  for sent in sentenceList:
    #List of words and punctuations
    wordList = tokenizer.tokenize(sent)

    #Comparing words with case insensitivity
    for word in wordList:
      if inpWord.lower() == word.lower():
        statements.append(sent)
        out.write(sent + "\n\n")

  out.write("\n\n\n---THE END---\n\n\nSYNONYMS-->\n")

  #Sentences consisting of synonyms

  def synCompare():
    #List of words and punctuations
    wordList = tokenizer.tokenize(sent)

    #Retrieving synonyms of the word not equal to the word
    for synWords in synonyms:
      for word in wordList:
        if synWords.lower() == word.lower() and synWords.lower() != inpWord.lower():
          statements.append(sent)
          out.write("\n\n" + sent)
          return

  for sent in sentenceList:
    synCompare()

  #Removing stop words

  out.write("\n\n\n---THE END---\n\n\nAFTER REMOVING STOP WORDS-->\n\n\n")

  stopRemoved = []
  englishStop = set(stopwords.words('english'))

  for lines in statements:
    wordList = tokenizer.tokenize(lines)

    #Appending words in the list excluding the stop words
    stopRemoved.append([word for word in wordList if word not in englishStop])

  #Writing the words as a string
  for words in stopRemoved:
    out.write(" ".join(words))


  inp.close()
  out.close()

def main():
  comprehensive(sys.argv[1], sys.argv[2], sys.argv[3])

if __name__ == '__main__':
  main()