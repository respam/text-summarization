'''
Created on 13-Jul-2012

@author: spam
'''
import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet
import collections
import re

def context():
  words = []
  stripped = []
  hypWord = []
  con = []

  lmtzr = WordNetLemmatizer()
  inp = open("test.txt",'r')
  readFile = inp.read()

  wordList = nltk.word_tokenize(readFile.lower())

  taglist = nltk.pos_tag(wordList)
  for each in taglist:
    if each[1] in ("NN","NNS"):
      words.append(each[0])

  # print words
  for i in words:

    match = re.search(r'[\w+][^\w+][\w+]',i)

    if match:
      words.remove(i)

    elif i[-1] == ".":
      index = words.index(i)
      j = i.replace(i[-1],"")
      words.remove(i)
      words.insert(index, j)

    elif i[0] == ".":
      index = words.index(i)
      j = i.replace(i[0],"")
      words.remove(i)
      words.insert(index, j)

  for each in words:
    strip = lmtzr.lemmatize(each)
    stripped.append(strip)

  for each in stripped:
    testWord = each + ".n.01"
    try:
      syn = wordnet.synset(testWord)
      hypernyms = syn.hypernym_paths()
      hyp = hypernyms[0][-3]
      hypWord.append(hyp)
    except:
      pass

  counter = collections.Counter(hypWord)
  final = counter.most_common(None)
  maximum = final[0][1]

  for each in final:
    if each[1] in [maximum, maximum - 1, maximum - 2, maximum - 3]:
      con.append(each[0].lemmas[0].name)

  print("Context in text:\n")

  for each in con:
    print (each + "\n")
    testWord = each

def main():
  context()

if __name__ == '__main__':
    main()
