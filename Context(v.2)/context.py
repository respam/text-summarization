'''
Created on 17-Dec-2012

@author: spam
'''

import string
import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.corpus import wordnet
from nltk.tokenize import sent_tokenize


def corpusImport():
    global sentences
    sentences = []  # Tokenized Sentences
    words = []  # Tokenized words not stop-words
    inp = open("Global_warming.txt", 'r')
    readInput = inp.read()
    englishStop = set(stopwords.words('english'))

    sentences = sent_tokenize(readInput)
    allWords = nltk.word_tokenize(readInput.lower())

    for each in allWords:
        if each not in englishStop:
            words.append(each)

#Removes punctuation symbols from each word and storing in the list "word"
def remSymbols(words):
    global exclude
    exclude = set(string.punctuation)
    count = 0
    for each in words:
        words[count] = ''.join(ch for ch in each if ch not in exclude)
        count = count + 1

#Lemmatizes using wordnet lemmatizer and stores in the list "word"
def lemmatize(words):
    lmtzr = WordNetLemmatizer()
    count = 0
    for each in words:
        words[count] = lmtzr.lemmatize(each)
        count = count + 1

#POS tagging of words in a sentence and storing them in the list "words"
def posTag(sent):
    text = nltk.word_tokenize(sent)
    taglist = nltk.pos_tag(text)
    del words[:]
    for each in taglist:
        if each[1] in ('NN', 'NNS') and len(each[0]) > 1:
            words.append(each[0])


def shortestDistance():
    
    global fLinked
    global finalLinked
    finalLinked = []    # List of list of closest nouns
    fLinked = []    # List of list of longest lists in finalLinked
    linked = []
    temp = list(set(words))
    max = 0
    del words[:]
    for each in temp:
        try:
            w = each.lower() + ".n.01"
            syn = wordnet.synset(w)
            words.append(each)
        except:
            pass
    # nouns = list(words)  # Nouns common in both wordnet and input
    for word in words:
        del linked[:]
        min = 999999999
        linked.append(word)
        w1 = word + ".n.01"
        syn1 = wordnet.synset(w1)
        for each in words:
            if each != word:
                w2 = each + ".n.01"
                syn2 = wordnet.synset(w2)
                dist = syn1.shortest_path_distance(syn2)
                if dist < min:
                    min = dist
                    del linked[:]
                    linked.append(word)
                    linked.append(each)
                elif min == dist:
                    linked.append(each)
        linked.append(min)
        
        finalLinked.append(list(linked))

        #List containing the lists with
        #maximum number of nouns closest to each other
        if len(linked) > max:
            max = len(linked)
            del fLinked[:]
            fLinked.append(list(linked))

        elif len(linked) == max:
            fLinked.append(list(linked))

def context_words():    # To be improvised
    global cWords
    cWords = []
    for each in finalLinked:
        for l in fLinked:
            distance = l[-1]
            if each[0] in l[1:]:
                if each[-1] <= distance:
                    fLinked.append(list(each))

    for each in fLinked:
        for w in each[:-1]:
            cWords.append(w)

    cWords = list(set(cWords))
    print cWords
    print len(cWords)


def summ_gen():     # To be modified
    for each in sent_words:
        for w in each[1:]:
            if w in cWords:
                out.write(each[0] + " ")


def in_sentence():
    global words
    global sent_words
    sent_words = []
    for each in sentences:
        temp = []
        words = []
        posTag(each)
        remSymbols(words)
        lemmatize(words)
        temp.append(each)
        temp.extend(words)
        sent_words.append(temp)
    # print sent_words
    del words[:]
    for s in sent_words:
        words.extend(s[1:])
    # print words


def main():
    global out
    out = open("out.txt", 'w')
    corpusImport()
    in_sentence()
    shortestDistance()
    context_words()
    # summ_gen()
    # out.close()

if __name__ == '__main__':
    main()
