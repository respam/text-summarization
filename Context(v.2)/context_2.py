'''
Created on 22-Feb-2013

@author: spam
'''

import string
import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.corpus import wordnet
from nltk.tokenize import sent_tokenize

def corpusInput():
    inp = open("Global_warming.txt",'r')
    readInput = inp.read()
    return readInput


def rem_symbols(word):
    exclude = list(set(string.punctuation))
    new_word = ''.join(ch for ch in word if ch not in exclude)
    return new_word

def stop_rem(word):
    englishStop = list(set(stopwords.words('english')))
    if word not in englishStop:
        return False
    else:
        return True

def posTag(sentence):
    words = []
    text = nltk.word_tokenize(sentence)
    tag = nltk.pos_tag(text)
    for each in tag:
        if each[1] in ('NN', 'NNS') and len(each[0]) > 1:
            words.append(each[0])
    return words

def lemmatize(word):
    lmtzr = WordNetLemmatizer()
    return lmtzr.lemmatize(word)

def preProcessing(readInput):
    sent_words = []
    sentences = sent_tokenize(readInput)
    for each in sentences:
        sen = []
        # fin_words = []
        sen.append(each)
        # wor = nltk.word_tokenize(each.lower())
        wor = posTag(each)
        for w in wor:
            if stop_rem(w) is False:
                w = rem_symbols(w)
                w = lemmatize(w)
                # fin_words.append(w)
                sen.append(w)
        # sen.extend(list(fin_words))
        temp = list(sen)
        sent_words.append(temp)
    return sent_words


def nounCluster(words):
    finalLinked = []    # List of list of closest nouns
    fLinked = []    # List of list of longest lists in finalLinked
    linked = []
    temp = list(set(words))
    max = 0
    del words[:]
    for each in temp:
        try:
            w = each.lower() + ".n.01"
            syn = wordnet.synset(w)
            words.append(each)
        except:
            pass

    for word in words:
        del linked[:]
        min = 999999999
        linked.append(word)
        w1 = word + ".n.01"
        syn1 = wordnet.synset(w1)
        for each in words:
            if each != word:
                w2 = each + ".n.01"
                syn2 = wordnet.synset(w2)
                dist = syn1.shortest_path_distance(syn2)
                if dist < min:
                    min = dist
                    del linked[:]
                    linked.append(word)
                    linked.append(each)
                elif min == dist:
                    linked.append(each)
        linked.append(min)
        finalLinked.append(list(linked))

        #List containing the list with
        #maximum number of nouns closest to each other
        if len(linked) > max:
            max = len(linked)
            del fLinked[:]
            fLinked.append(list(linked))

        elif len(linked) == max:
            fLinked.append(list(linked))
    print fLinked

def main():
    words = []
    ri = corpusInput()
    sw = preProcessing(ri)
    for each in sw:
        for w in each[1:]:
            words.append(w.lower())
    nounCluster(words)


if __name__ == '__main__':
    main()
