'''
Created on 25-Jul-2012

@author: spam
'''
import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet
from nltk.tokenize import sent_tokenize
import collections
import re

def context():
  words = []
  stripped = []
  hypWord = []
  con = []
  
  lmtzr = WordNetLemmatizer()
  inp = open("obama.txt",'r')
  out = open("out.txt",'w')
  readFile = inp.read()
  
  sentenceList = sent_tokenize(readFile)
  wordList = nltk.word_tokenize(readFile.lower())

  taglist = nltk.pos_tag(wordList)
  for each in taglist:
    if each[1] in ("NN","NNS","NNP"):
      words.append(each[0])
      
  def stripping(i):
    match=re.search(r'[\w+][^\w+][\w+]',i)
    
    if match:
      words.remove(i)
    
    elif i[-1] == ".":
      index = words.index(i)
      j = i.replace(i[-1],"")
      words.remove(i)
      words.insert(index, j)
      
    elif i[0] == ".":
      index = words.index(i)
      j = i.replace(i[0],"")
      words.remove(i)
      words.insert(index, j)
      
  for each in words:
    strip = lmtzr.lemmatize(each)
    stripped.append(strip)

  for i in words:
    stripping(i)

  for each in stripped:
    testWord = each +".n.01"
    try:
      syn = wordnet.synset(testWord) #@UndefinedVariable
      hypernyms = syn.hypernym_paths()
      hyp = hypernyms[0][-2]
      hypWord.append(hyp.lemmas[0].name)
    except:
      pass

  counter = collections.Counter(hypWord)
  final = counter.most_common(None)
  maximum = final[0][1]
  
  for each in final:
    if each[1] in [maximum, maximum-1, maximum-2, maximum-3]:
      con.append(each[0])
      
  def finalContext(sentence):
    wordList = nltk.word_tokenize(sentence.lower())
    taglist = nltk.pos_tag(wordList)
    
    for each in taglist:
      test = each[0]
      if each[1] in ("NN","NNS","NNP"):
        match=re.search(r'[\w+][^\w+][\w+]',each[0])
        if not match:
          if each[0][-1] == ".":
            test = each[0][:-1]
          elif each[0][0] == ".":
            test = each[0][0:]
            
          test = lmtzr.lemmatize(test)
          testWord = test +".n.01"
          
          try:
            syn = wordnet.synset(testWord) #@UndefinedVariable
            hypernyms = syn.hypernym_paths()
            hyp = hypernyms[0][-2]
            if hyp.lemmas[0].name in con:
              out.write(sentence)
              return
          except:
            pass

  for sentence in sentenceList:
    finalContext(sentence)
        
def main():
  context()

if __name__ == '__main__':
    main()
