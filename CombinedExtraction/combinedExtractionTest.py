'''
Created on 01-Aug-2012

@author: spam
'''

from __future__ import division
import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet
from nltk.tokenize import sent_tokenize
import collections
import re

def context():
  
  words = []
  stripped = []
  hypWord = []
  final = []
  
  lmtzr = WordNetLemmatizer()
  inpFile = open("newCombinedInput.txt", 'r')
  readFile = inpFile.read()
  
  sentenceList = sent_tokenize(readFile)
  wordList = nltk.word_tokenize(readFile.lower())
  
  taglist = nltk.pos_tag(wordList)
  for each in taglist:
    if each[1] in ("NN","NNS","NNP"):
      words.append(each[0])
      
  def stripping(i):
    match=re.search(r'[\w+][^\w+][\w+]',i)
    
    if match:
      words.remove(i)
    
    elif i[-1] == ".":
      index = words.index(i)
      j = i.replace(i[-1],"")
      words.remove(i)
      words.insert(index, j)
      
    elif i[0] == ".":
      index = words.index(i)
      j = i.replace(i[0],"")
      words.remove(i)
      words.insert(index, j)
      
  for i in words:
    stripping(i)
        
  for each in words:
    strip = lmtzr.lemmatize(each)
    stripped.append(strip)

    
  for each in stripped:
    testWord = each +".n.01"
    try:
      syn = wordnet.synset(testWord) #@UndefinedVariable
      hypernyms = syn.hypernym_paths()
      hyp = hypernyms[0][-2]
      hypWord.append(hyp.lemmas[0].name)
    except:
      pass
    
  counter = collections.Counter(hypWord)
  testFinal = counter.most_common(None)
    
  for (word,num) in testFinal: #@UnusedVariable
    final.append(word)
    
  
  def rest():
    
      hypWord = []
      finalOutput = []
      def finalContext(sentence):
        wordList = nltk.word_tokenize(sentence.lower())
        taglist = nltk.pos_tag(wordList)
        
        for each in taglist:
          test = each[0]
          if each[1] in ("NN", "NNP", "NNS"):
            match = re.search(r'[\w+][^\w+][\w+]',each[0])
            if not match:
              if each[0][-1] == ".":
                test = each[0][:-1]
              elif each[0][0] == ".":
                test = each[0][0:]
                
              test = lmtzr.lemmatize(test)
              testWord = test + ".n.01"
              
              try:
                syn = wordnet.synset(testWord) #@UndefinedVariable
                hypernyms = syn.hypernym_paths()
                hyp = hypernyms[0][-2]
                if hyp.lemmas[0].name == final[0]:
                  finalOutput.append(sentence)
                  print (sentence)
                  return
              except:
                pass
  
      def filling():
        for sentence in sentenceList:
          finalContext(sentence)
      
      filling()
      
      if finalOutput != []:
        for each in sentenceList:
          if finalOutput[0] == each:
            begin = sentenceList.index(each)
          elif finalOutput[-1] == each:
            end = sentenceList.index(each)
        
        deducted = sentenceList [begin:end + 1]    
        del sentenceList [begin:end + 1]
        
        for each in deducted:
          wordList = nltk.word_tokenize(each.lower())
          taglist = nltk.pos_tag(wordList)
          
          for each in taglist:
            test = each[0]
            if each[1] in ("NN", "NNS", "NNP"):
              match = re.search(r'[\w+][^\w+][\w+]',each[0])
              if not match:
                if each[0][-1] == ".":
                  test = each[0][:-1]
                elif each[0][0] == ".":
                  test = each[0][0:]
                  
                test = lmtzr.lemmatize(test)
                testWord = test + ".n.01"
                
                try:
                  syn = wordnet.synset(testWord) #@UndefinedVariable
                  hypernyms = syn.hypernym_paths()
                  hyp = hypernyms[0][-2]
                  if hyp.lemmas[0].name in final:
                    hypWord.append(hyp.lemmas[0].name)
                except:
                  pass
                         
        for each in final:
          if each in hypWord:
            try:
              final.remove(each)
            except:
              pass
           
        print ("-" * 50)
        rest()
      
  rest()
  print "\n\n--THE END--"
  

def main():
  context()

if __name__ == '__main__':
    main()