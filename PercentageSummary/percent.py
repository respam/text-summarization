'''
Created on 21-Sep-2012

@author: spam
'''
from __future__ import division
import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet
from nltk.tokenize import sent_tokenize
import collections
import re
import math

def context(inpFile,percent):
  words = []
  stripped = []
  hypWord = []
  con = []
  summary = []
  
  lmtzr = WordNetLemmatizer()
  inp = open(inpFile,'r')
  readFile = inp.read()
  
  sentenceList = sent_tokenize(readFile)
  wordList = nltk.word_tokenize(readFile.lower())
  
  taglist = nltk.pos_tag(wordList)
  for each in taglist:
    if each[1] in ("NN", "NNS"):
      words.append(each[0])
      
  def stripping(i):
    match = re.search(r'[\w+][^\w][\w+]',i)
    
    if match:
      words.remove(i)
      
    elif i[-1] == ".":
      index = words.index(i)
      j = i.replace(i[-1],"")
      words.remove(i)
      words.insert(index,j)
      
    elif i[0] == ".":
      index = words.index(i)
      j = i.replace(i[0],"")
      words.remove(i)
      words.insert(index,j)
      
  for i in words:
    stripping(i)
    
  for each in words:
    strip = lmtzr.lemmatize(each)
    stripped.append(strip)
    
  for each in stripped:
    testWord = each + ".n.01"
    try:
      syn = wordnet.synset(testWord) #@UndefinedVariable
      hypernyms = syn.hypernym_paths()
      hyp = hypernyms[0][-2]
      hypWord.append(hyp.lemmas[0].name)
    except:
      pass
    
  counter = collections.Counter(hypWord)
  final = counter.most_common(None)
  maximum = final[0][1]
  
  for each in final:
    if each[1] in [maximum, maximum-1, maximum-2, maximum-3]:
      con.append(each[0])

  def finalContext(sentence):
    wordList = nltk.word_tokenize(sentence.lower())
    taglist = nltk.pos_tag(wordList)
    
    for each in taglist:
      test = each[0]
      if each[1] in ("NN", "NNS"):
        match = re.search(r'[\w+][^\w+][\w+]',each[0])
        
        if not match:
          if each[0][-1] == ".":
            test = each[0][:-1]
          elif each[0][0] == ".":
            test = each[0][0:]
            
          test = lmtzr.lemmatize(test)
          testWord = test + ".n.01"
          
          try:
            syn = wordnet.synset(testWord) #@UndefinedVariable
            hypernyms = syn.hypernym_paths()
            hyp = hypernyms[0][-2]
            if hyp.lemmas[0].name in con:
              summary.append(sentence)
              return
          except:
            pass
          
  for sentence in sentenceList:
    finalContext(sentence)
  
  sumLen = len(summary)
  print "sumLen = " + str(sumLen)
  outNo = int(math.floor((percent / 100) * sumLen))
  print "outNo = " + str(outNo) 
  lineNo = int(math.ceil(sumLen / outNo))
  print "lineNo = " + str(lineNo)
  
  print (summary[0])
  
  for each in summary:
    if each not in (summary[0], summary[sumLen - 1]):
      senIndex = summary.index(each)
      
      if senIndex % lineNo is 0:
        print (each)
        
  print (summary[sumLen - 1])
  
def main():
  
  fileName = "obama.txt"
  percent = 50
  
  context(fileName,percent)

if __name__ == '__main__':
    main()