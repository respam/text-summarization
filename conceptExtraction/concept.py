'''
Created on 02-Nov-2012

@author: spam
'''

import os
import re
import string
import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import *
from nltk.tokenize import sent_tokenize
import collections

def stripping(words):
  exclude = set(string.punctuation)
  count = 0
  for each in words:
    words[count] = ''.join(ch for ch in each if ch not in exclude)
    count = count + 1
    
def main():
  
  global sentences
  global words
  sentences = []
  words = []
  stripped = []
  path = "C:\\Users\\spam\\workspace\\nlpResearch\\inputs\\testInputs" # Path of the input files
  files = os.listdir(path)
  lmtzr = WordNetLemmatizer()
  englishStop = stopwords.words('english')
  print englishStop
  
  # Processing each file in the input directory
  for each in files:
    inp = open(path + "\\" + each,'r')
    readFile = inp.read()
    
    sentenceList = sent_tokenize(readFile)
    wordList = nltk.word_tokenize(readFile.lower())
    for each in sentenceList:
      sentences.append(each)
    for each in wordList:
      if each not in englishStop:
        words.append(each)
  
  stripping(words) #Remove punctuation marks and symbols
  
  # Lemmatizing
  for every in words:
    strip = lmtzr.lemmatize(every)
    stripped.append(strip)
  
if __name__ == '__main__':
    main()
