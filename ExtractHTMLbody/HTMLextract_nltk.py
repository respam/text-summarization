'''
Created on 21-Sep-2012

@author: spam
'''
import nltk
from urllib import urlopen

def extractBody():
  
  i = 1
  urls = []
  file = open('links.txt','r')
  urls = file.readlines()
  
  for each in urls:
    out = str(i) + ".txt"
    html = urlopen(each).read()
    raw = nltk.clean_html(html)
    outOpen = open("./extracted/" + out,'w')
    outOpen.write(raw)
    i = i + 1
    
  file.close()
  outOpen.close()

def main():
  extractBody()

if __name__ == '__main__':
    main()