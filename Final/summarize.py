'''
Created on 09-Nov-2012

@author: spam
'''

import os
import string
import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import *
from nltk.tokenize import sent_tokenize
import collections

def corpusImport():
  global doc
  global files
  global path
  path = "C:\\Users\\spam\\nlp_python workspace\\nlpResearch\\inputs\\testInputs"  # Path of the input files
  files = os.listdir(path)  # Contains all the files in the specified input folder
  doc = len(files)  # Number of input documents

def preProcessing():
  global sentenceList
  global wordList
  englishStop = stopwords.words('english')
  for each in files:
    inp = open(path + "\\" + each,'r')
    readFile = inp.read()

    sentenceList = sent_tokenize(readFile)  # Original Sentence List
    wordList = nltk.word_tokenize(readFile.lower())  # Original Word LIst
    for each in sentenceList:
      sentences.append(each)
    for each in wordList:
      if each not in englishStop:
        words.append(each)

def remSymbols():
  exclude = set(string.punctuation)
  count = 0
  for each in words:
    words[count] = ''.join(ch for ch in each if ch not in exclude)
    count = count + 1

def lemmatize():
  lmtzr = WordNetLemmatizer()
  for each in words:
    strip = lmtzr.lemmatize(each)
    stripped.append(strip)

def posTagging():
  global tagged
  taglist = nltk.pos_tag(stripped)
  tagged = []
  for each in taglist:
    if each[1] in ('NN','NNS') and len(each[0]) > 1:
      tagged.append(each[0])

def nounCollection():
  global maxNoun
  counter = collections.Counter(tagged)
  final = counter.most_common(None)  # List with all the nouns and their frequency
  maxNoun = final[0][0]  # Noun with the maximum occurence
  print final
  print maxNoun

#def tfidf():
#
#
#def isf():
#

def main():
  global sentences
  global words
  global stripped
  sentences = []
  words = []
  stripped = []
  corpusImport()
  preProcessing()
  remSymbols()
  lemmatize()
  posTagging()
  nounCollection()
#  if doc > 1:
#    tfidf()
#  else:
#    tfisf()

if __name__ == '__main__':
    main()
