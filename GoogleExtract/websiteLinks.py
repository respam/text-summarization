'''
Created on 21-Sep-2012

@author: spam
'''
import time,random
from xgoogle.search import GoogleSearch, SearchError

def links():
  f = open('links.txt','wb')
  
  for i in range(0,2000):
    wt = random.uniform(2,5)
    gs = GoogleSearch("hospital")
    gs.result_per_page = 10
    gs.page = i
    results = gs.get_results()
    
    ##Try not giving a random short wait
    time.sleep(wt)
    print 'This is the %dth iteration and waited %f seconds' % (i,wt)
    
    for res in results:
      f.write(res.url.encode("utf8"))
      f.write("\n")
      
  print "-----COMPLETE-----"
  f.close()

def main():
  links()

if __name__ == '__main__':
    main()