#@PydevCodeAnalysisIgnore
#-------------------------------------------------------------------------------
# Name:        Distance and Similarity between two words in wordnet
# Purpose:
#
# Author:      spam
#
# Created:     06-06-2012
# Copyright:   (c) spam 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import sys
from nltk.corpus import wordnet

def distance():
  word1 = "cat"
  word2 = "dog"
  
  w1 = word1 + ".n.01"
  w2 = word2 + ".n.01"

  syn1 = wordnet.synset(w1)
  syn2 = wordnet.synset(w2)

  #Distance
  print "\nShortest distance Between " + word1 + " and " + word2 + ":\n"
  print syn1.shortest_path_distance(syn2)

  #Similarity
  print "\nSimilarity Between " + word1 + " and " + word2 + ":\n"
  print syn1.wup_similarity(syn2)

  #Common Hypernyms between the two words
  hyp = syn1.common_hypernyms(syn2)
  print "\nCommon Hypernyms between the two words:\n"
  print hyp


def main():
  distance()

if __name__ == '__main__':
  main()
