function sentencerank(file)
fid = fopen('sent_rank.txt','w')
fis = readfis('summary')
inp = load(file)
out = evalfis(inp,fis)
[nrows,ncols] = size(out)
for row=1:nrows
    out1(row,:) = [row out(row,:)]
end
out2 = sortrows(out1,-2)
[rows,cols] = size(out2)
for row=1:rows
    fprintf(fid,'%d %d',out2(row,:))
    fprintf(fid,'\n')
end
fclose(fid)