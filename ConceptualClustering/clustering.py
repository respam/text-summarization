'''
Created on 17-Aug-2012

@author: spam
'''

import sgmllib
import string

class FoundBody(Exception):
  pass

class ExtractBody(sgmllib.SGMLParser):
  
  def __init__(self, verbose=0):
    sgmllib.SGMLParser.__init__(self, verbose)
    self.body = self.data = None
    
  def handle_data(self, data):
    if self.data is not None:
      self.data.append(data)
      
  def start_body(self, attrs):
    self.data = []
    
  def end_body(self):
    self.body = string.join(self.data, "")
    raise FoundBody


def extract(inp_file):
  p = ExtractBody()
  try:
    while 1:
      s = inp_file.read()
      if not s:
        break
      p.feed(s)
    p.close
  except:
    return p.body
  return None


def main():
  for each in["reut2-000.sgm", "reut2-001.sgm", "reut2-002.sgm", "reut2-003.sgm", "reut2-004.sgm"
              , "reut2-005.sgm", "reut2-006.sgm", "reut2-007.sgm", "reut2-008.sgm", "reut2-009.sgm"
              , "reut2-010.sgm", "reut2-011.sgm", "reut2-012.sgm", "reut2-013.sgm", "reut2-014.sgm"
              , "reut2-015.sgm", "reut2-016.sgm", "reut2-017.sgm", "reut2-018.sgm", "reut2-019.sgm"
              , "reut2-020.sgm", "reut2-021.sgm"]:
    test = "reuters21578/" + each
    print extract(open(test))

if __name__ == '__main__':
    main()