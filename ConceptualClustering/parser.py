'''
Created on 17-Aug-2012
@author: Jagrity
'''
import sgmllib
import string

List=[]
data=[]
body=[]
class FoundBody(Exception):
    pass

class ExtractBody(sgmllib.SGMLParser):
    body = data=None
    def __init__(self, verbose=0):
        sgmllib.SGMLParser.__init__(self, verbose)

    def handle_data(self, data):
        if self.data is not None:
            self.data.append(data)

    def start_body(self, attrs):
        if self.data is None:
          self.data = []

    def end_body(self):
        self.body = string.join(self.data, "")
        #raise FoundBody

def extract(file):
    p = ExtractBody()
    try:
        while file:
            # read small chunks
            s = file.read()        
            if not s:
                break
            p.feed(s)  
        p.close()
        raise FoundBody      
    except FoundBody:
        return p.body
    return None

fp=open("reuters21578/reut2-000.sgm")
fp1=open("out.txt","w")
fp1.write(extract(fp))
fp1.close()